﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AmlakSystem
{
    public partial class EditMelk : Form
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-FGMP2O3;Initial Catalog=MoshaverAmlak;Integrated Security=True");
        string b;
        string d;
        public EditMelk(string a,string c)
        {
            InitializeComponent();
            b = a;
            d = c;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == string.Empty && textBox2.Text == string.Empty && textBox3.Text == string.Empty && textBox4.Text == string.Empty && textBox5.Text == string.Empty)
                MessageBox.Show("فیلدی را جهت ویرایش پر کنید", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
             
             if(textBox1.Text!=string.Empty)
            {
                con.Open();
                SqlCommand com = new SqlCommand("update Melk set Mantaghe=@p2 where PCode=@p1", con);
                com.Parameters.Add(new SqlParameter("@p1", b));
                com.Parameters.Add(new SqlParameter("@p2", textBox1.Text));
                com.ExecuteScalar();
                con.Close();
               // MessageBox.Show("ویرایش با موفقیت انجام شد", "موفق", MessageBoxButtons.OK);
            }
             if(textBox2.Text!=string.Empty)
            {
                con.Open();
                SqlCommand com = new SqlCommand("update Melk set [Address]=@p3 where PCode=@p1", con);
                com.Parameters.Add(new SqlParameter("@p1",b));
                com.Parameters.Add(new SqlParameter("@p3", textBox2.Text));
                com.ExecuteScalar();
                con.Close();
               // MessageBox.Show("ویرایش با موفقیت انجام شد", "موفق", MessageBoxButtons.OK);
            }
             if (textBox3.Text != string.Empty)
            {
                con.Open();
                SqlCommand com = new SqlCommand("update Melk set Metrazh=@p4 where PCode=@p1", con);
                com.Parameters.Add(new SqlParameter("@p1", b));
                com.Parameters.Add(new SqlParameter("@p4", textBox3.Text));
                com.ExecuteScalar();
                con.Close();
               // MessageBox.Show("ویرایش با موفقیت انجام شد", "موفق", MessageBoxButtons.OK);
            }
             if (textBox4.Text != string.Empty)
            {
                con.Open();
                SqlCommand com = new SqlCommand("update Melk set [Bedroom Number]=@p5 where PCode=@p1", con);
                com.Parameters.Add(new SqlParameter("@p1", b));
                com.Parameters.Add(new SqlParameter("@p5", textBox4.Text));
                com.ExecuteScalar();
                con.Close();
              //  MessageBox.Show("ویرایش با موفقیت انجام شد", "موفق", MessageBoxButtons.OK);
            }
             if (textBox5.Text != string.Empty)
            {
                con.Open();
                SqlCommand com = new SqlCommand("update Melk set Price=@p6 where PCode=@p1", con);
                com.Parameters.Add(new SqlParameter("@p1", b));
                com.Parameters.Add(new SqlParameter("@p6", textBox5.Text));
                com.ExecuteScalar();
                con.Close();
               // MessageBox.Show("ویرایش با موفقیت انجام شد", "موفق", MessageBoxButtons.OK);
            }
            MessageBox.Show("ویرایش با موفقیت انجام شد", "موفق", MessageBoxButtons.OK);
            Account f10 = new Account(d);
            f10.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Account f2 = new Account(d);
            f2.Show();
            this.Close();
        }

        private void EditMelk_Load(object sender, EventArgs e)
        {

        }
    }
}
