﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using AmlakSystem;

namespace Amlak_System
{
    public partial class MPayment : Form
    {
        SqlConnection con = new SqlConnection("Data Source=Localhost;Initial Catalog=MoshaverAmlak;Integrated Security=True");
        SqlCommand com;
        SqlCommand com2;
        SqlCommand com3;
        String PCode_Melk;
        String Email_Kharidar;
        SqlDataReader DataR;
        String Email_Seller;
        String time;
        String date1;
        public MPayment(String ST1, String Email)
        {

            PCode_Melk = ST1;
            Email_Kharidar = Email;
            InitializeComponent();
            timer1.Start();
        }
        public string date()
        {
            string Result;
            System.Globalization.PersianCalendar datepersian = new System.Globalization.PersianCalendar();
            System.String Year, Month, Day;
            DateTime date_now = DateTime.Now;
            Year = datepersian.GetYear(date_now).ToString();
            Month = datepersian.GetMonth(date_now).ToString();
            Day = datepersian.GetDayOfMonth(date_now).ToString();
            return Result = Year + "-" + Month + "-" + Day;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            con.Close();
            con.Open();
            com2 = new SqlCommand("select Email from Melk where PCode=@p1;", con);
            com2.Parameters.Add(new SqlParameter("@p1", PCode_Melk));
            DataR = com2.ExecuteReader();
            DataR.Read();
            Email_Seller = DataR["Email"].ToString();
            DataR.Close();
            com = new SqlCommand("insert into [Melk Reservation](PCode,[Kharidar Email],[Seller Email],RDate,RTime) values (@p1,@p2,@p3,@p4,@p5);", con);
            com.Parameters.Add(new SqlParameter("@p1", PCode_Melk));
            com.Parameters.Add(new SqlParameter("@p2", Email_Kharidar));
            com.Parameters.Add(new SqlParameter("@p3", Email_Seller));
            com.Parameters.Add(new SqlParameter("@p4", date1.ToString()));
            com.Parameters.Add(new SqlParameter("@p5", time.ToString()));
            com.ExecuteNonQuery();
            com3 = new SqlCommand("update Melk set Reserved='yes' where PCode=@p1;", con);
            com3.Parameters.Add(new SqlParameter("@p1", PCode_Melk));
            com3.ExecuteNonQuery();
            con.Close();
            MainPage f2 = new MainPage();
            f2.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            con.Close();
            con.Open();
            com2 = new SqlCommand("delete from Kharidar where Email=@p1;", con);
            com2.Parameters.Add(new SqlParameter("@p1", Email_Kharidar));
            com2.ExecuteNonQuery();
            com = new SqlCommand("delete from Shakhs where Email=@p1;", con);
            com.Parameters.Add(new SqlParameter("@p1", Email_Kharidar));
            com.ExecuteNonQuery();
            MainPage f1 = new MainPage();
            f1.Show();
            this.Close();
        }

        private void MPayment_Load(object sender, EventArgs e)
        {
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            time = DateTime.Now.ToString();
            date1 = date();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            con.Close();
            con.Open();
            com2 = new SqlCommand("select Email from Melk where PCode=@p1;", con);
            com2.Parameters.Add(new SqlParameter("@p1", PCode_Melk));
            DataR = com2.ExecuteReader();
            DataR.Read();
            Email_Seller = DataR["Email"].ToString();
            DataR.Close();
            time = DateTime.Now.ToString();
            date1 = date();
            com = new SqlCommand("insert into [Melk Reservation](PCode,[Kharidar Email],[Seller Email],RDate,RTime) values (@p1,@p2,@p3,@p4,@p5);", con);
            com.Parameters.Add(new SqlParameter("@p1", PCode_Melk));
            com.Parameters.Add(new SqlParameter("@p2", Email_Kharidar));
            com.Parameters.Add(new SqlParameter("@p3", Email_Seller));
            com.Parameters.Add(new SqlParameter("@p4", date1.ToString()));
            com.Parameters.Add(new SqlParameter("@p5", time.ToString()));
            com.ExecuteNonQuery();
            com3 = new SqlCommand("update Melk set Reserved='yes' where PCode=@p1;", con);
            com3.Parameters.Add(new SqlParameter("@p1", PCode_Melk));
            com3.ExecuteNonQuery();
            con.Close();
            MainPage f2 = new MainPage();
            f2.Show();
            this.Close();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            con.Close();
            con.Open();
            com2 = new SqlCommand("delete from Kharidar where Email=@p1;", con);
            com2.Parameters.Add(new SqlParameter("@p1", Email_Kharidar));
            com2.ExecuteNonQuery();
            com = new SqlCommand("delete from Shakhs where Email=@p1;", con);
            com.Parameters.Add(new SqlParameter("@p1", Email_Kharidar));
            com.ExecuteNonQuery();
            MainPage f1 = new MainPage();
            f1.Show();
            this.Close();
        }
    }
}
