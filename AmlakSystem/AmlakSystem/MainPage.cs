﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Amlak_System;

namespace AmlakSystem
{
    public partial class MainPage : Form
    {
        SqlConnection con = new SqlConnection("Data Source=Localhost;Initial Catalog=MoshaverAmlak;Integrated Security=True");
        SqlCommand com;
        public SqlDataReader DataR;
        public MainPage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int i = 0;
            try
            {
                if((textBox1.Text==string.Empty)|| (textBox2.Text==string.Empty))
                {
                    MessageBox.Show("لطفا نام کاربری و رمز عبور را وارد کنید","خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                com = new SqlCommand("select count(*) from Seller where Email='" + textBox1.Text + "' and Password='" + textBox2.Text + "'", con);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                    i = (int)com.ExecuteScalar();
                }
                con.Close();
                if (i > 0)
                {
                    MessageBox.Show("ورود موفقیت آمیز بود");
                    Account f1 = new Account(textBox1.Text);
                    f1.Show();
                    this.Hide();
                }
                else
                    MessageBox.Show("نام کاربری و یا رمز عبور اشتباه می باشد", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                i = 0;
                con.Close();
            }
        }

        private void MainPage_Load(object sender, EventArgs e)
        {
            con.Close();
            con.Open();
            com = new SqlCommand("select distinct Ostan from Melk where Reserved = 'no';", con);
            DataR = com.ExecuteReader();
            if (DataR.HasRows)
            {
                while (DataR.Read())
                {
                    comboBox1.Items.Add(DataR["Ostan"]);
                }
            }
            DataR.Close();
            con.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != "" && comboBox2.Text == "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("select * from Melk where Ostan=@p1 and Reserved='no';", con);
                com.Parameters.Add(new SqlParameter("@p1", comboBox1.Text));
                DataR = com.ExecuteReader();
                Data df = new Data(DataR);
                df.Show();
                this.Hide();
            }
            else if (comboBox1.Text != "" && comboBox2.Text != "" && comboBox3.Text == "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("select * from Melk where Ostan = @p1 and Shahr = @p2 and Reserved = 'no';", con);
                com.Parameters.Add(new SqlParameter("@p1", comboBox1.Text));
                com.Parameters.Add(new SqlParameter("@p2", comboBox2.Text));
                DataR = com.ExecuteReader();
                Data df = new Data(DataR);
                df.Show();
                this.Hide();
            }
            else if (comboBox1.Text != "" && comboBox2.Text != "" && comboBox3.Text != "" && comboBox4.Text == "" && comboBox5.Text == "" && comboBox6.Text == "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("select * from Melk where Ostan = @p1 and Shahr = @p2 and Mantaghe = @p3 and Reserved = 'no';", con);
                com.Parameters.Add(new SqlParameter("@p1", comboBox1.Text));
                com.Parameters.Add(new SqlParameter("@p2", comboBox2.Text));
                com.Parameters.Add(new SqlParameter("@p3", comboBox3.Text));
                DataR = com.ExecuteReader();
                Data df = new Data(DataR);
                df.Show();
                this.Hide();
            }
            else if (comboBox1.Text != "" && comboBox2.Text != "" && comboBox3.Text != "" && comboBox4.Text != "" && comboBox5.Text == "" && comboBox6.Text == "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("select * from Melk where Ostan = @p1 and Shahr = @p2 and Mantaghe = @p3 and Price = @p4 and Reserved = 'no';", con);
                com.Parameters.Add(new SqlParameter("@p1", comboBox1.Text));
                com.Parameters.Add(new SqlParameter("@p2", comboBox2.Text));
                com.Parameters.Add(new SqlParameter("@p3", comboBox3.Text));
                com.Parameters.Add(new SqlParameter("@p4", comboBox4.Text));
                DataR = com.ExecuteReader();
                Data df = new Data(DataR);
                df.Show();
                this.Hide();
            }
            else if (comboBox1.Text != "" && comboBox2.Text != "" && comboBox3.Text != "" && comboBox4.Text == "" && comboBox5.Text != "" && comboBox6.Text == "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("select * from Melk where Ostan = @p1 and Shahr = @p2 and Mantaghe = @p3 and Metrazh = @p4 and Reserved = 'no';", con);
                com.Parameters.Add(new SqlParameter("@p1", comboBox1.Text));
                com.Parameters.Add(new SqlParameter("@p2", comboBox2.Text));
                com.Parameters.Add(new SqlParameter("@p3", comboBox3.Text));
                com.Parameters.Add(new SqlParameter("@p4", comboBox5.Text));
                DataR = com.ExecuteReader();
                Data df = new Data(DataR);
                df.Show();
                this.Hide();
            }
            else if (comboBox1.Text != "" && comboBox2.Text != "" && comboBox3.Text != "" && comboBox4.Text == "" && comboBox5.Text == "" && comboBox6.Text != "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("select * from Melk where Ostan = @p1 and Shahr = @p2 and Mantaghe = @p3 and Sazeh = @p4 and Reserved = 'no';", con);
                com.Parameters.Add(new SqlParameter("@p1", comboBox1.Text));
                com.Parameters.Add(new SqlParameter("@p2", comboBox2.Text));
                com.Parameters.Add(new SqlParameter("@p3", comboBox3.Text));
                com.Parameters.Add(new SqlParameter("@p4", comboBox6.Text));
                DataR = com.ExecuteReader();
                Data df = new Data(DataR);
                df.Show();
                this.Hide();
            }
            else if (comboBox1.Text != "" && comboBox2.Text != "" && comboBox3.Text != "" && comboBox4.Text != "" && comboBox5.Text != "" && comboBox6.Text == "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("select * from Melk where Ostan = @p1 and Shahr = @p2 and Mantaghe = @p3 and Price = @p4 and Metrazh = @p5 and Reserved = 'no';", con);
                com.Parameters.Add(new SqlParameter("@p1", comboBox1.Text));
                com.Parameters.Add(new SqlParameter("@p2", comboBox2.Text));
                com.Parameters.Add(new SqlParameter("@p3", comboBox3.Text));
                com.Parameters.Add(new SqlParameter("@p4", comboBox4.Text));
                com.Parameters.Add(new SqlParameter("@p5", comboBox5.Text));
                DataR = com.ExecuteReader();
                Data df = new Data(DataR);
                df.Show();
                this.Hide();
            }
            else if (comboBox1.Text != "" && comboBox2.Text != "" && comboBox3.Text != "" && comboBox4.Text != "" && comboBox5.Text == "" && comboBox6.Text != "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("select * from Melk where Ostan = @p1 and Shahr = @p2 and Mantaghe = @p3 and Price = @p4 and Sazeh = @p5 and Reserved = 'no';", con);
                com.Parameters.Add(new SqlParameter("@p1", comboBox1.Text));
                com.Parameters.Add(new SqlParameter("@p2", comboBox2.Text));
                com.Parameters.Add(new SqlParameter("@p3", comboBox3.Text));
                com.Parameters.Add(new SqlParameter("@p4", comboBox4.Text));
                com.Parameters.Add(new SqlParameter("@p5", comboBox6.Text));
                DataR = com.ExecuteReader();
                Data df = new Data(DataR);
                df.Show();
                this.Hide();
            }
            else if (comboBox1.Text != "" && comboBox2.Text != "" && comboBox3.Text != "" && comboBox4.Text == "" && comboBox5.Text != "" && comboBox6.Text != "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("select * from Melk where Ostan = @p1 and Shahr = @p2 and Mantaghe = @p3 and Metrazh = @p4 and Sazeh = @p5 and Reserved = 'no';", con);
                com.Parameters.Add(new SqlParameter("@p1", comboBox1.Text));
                com.Parameters.Add(new SqlParameter("@p2", comboBox2.Text));
                com.Parameters.Add(new SqlParameter("@p3", comboBox3.Text));
                com.Parameters.Add(new SqlParameter("@p4", comboBox5.Text));
                com.Parameters.Add(new SqlParameter("@p5", comboBox6.Text));
                DataR = com.ExecuteReader();
                Data df = new Data(DataR);
                df.Show();
                this.Hide();
            }
            else if (comboBox1.Text != "" && comboBox2.Text != "" && comboBox3.Text != "" && comboBox4.Text != "" && comboBox5.Text != "" && comboBox6.Text != "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("select * from Melk where Ostan = @p1 and Shahr = @p2 and Mantaghe = @p3 and Price = @p4 and Metrazh = @p5 and Sazeh = @p6 and Reserved = 'no';", con);
                com.Parameters.Add(new SqlParameter("@p1", comboBox1.Text));
                com.Parameters.Add(new SqlParameter("@p2", comboBox2.Text));
                com.Parameters.Add(new SqlParameter("@p3", comboBox3.Text));
                com.Parameters.Add(new SqlParameter("@p4", comboBox4.Text));
                com.Parameters.Add(new SqlParameter("@p5", comboBox5.Text));
                com.Parameters.Add(new SqlParameter("@p6", comboBox6.Text));
                DataR = com.ExecuteReader();
                Data df = new Data(DataR);
                df.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("فیلدی را جهت جستجو پر کنید");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Participation_Payment f2 = new Participation_Payment();
            f2.Show();
            this.Hide();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.Text = "";
            comboBox2.Items.Clear();
            comboBox3.Text = "";
            comboBox3.Items.Clear();
            comboBox4.Text = "";
            comboBox4.Items.Clear();
            comboBox5.Text = "";
            comboBox5.Items.Clear();
            comboBox6.Text = "";
            comboBox6.Items.Clear();
            con.Close();
            con.Open();
            com = new SqlCommand("select distinct Ostan,Shahr from Melk where Reserved = 'no';", con);
            DataR = com.ExecuteReader();
            if (DataR.HasRows)
            {
                while (DataR.Read())
                {
                    if (comboBox1.SelectedItem.ToString() == (String)DataR["Ostan"])
                    {
                        comboBox2.Items.Add(DataR["Shahr"]);
                    }
                }
            }
            DataR.Close();
            con.Close();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox3.Text = "";
            comboBox3.Items.Clear();
            comboBox4.Text = "";
            comboBox4.Items.Clear();
            comboBox5.Text = "";
            comboBox5.Items.Clear();
            comboBox6.Text = "";
            comboBox6.Items.Clear();
            con.Close();
            con.Open();
            com = new SqlCommand("select distinct Ostan,Shahr,Mantaghe from Melk where Reserved = 'no';", con);
            DataR = com.ExecuteReader();
            if (DataR.HasRows)
            {
                while (DataR.Read())
                {
                    if (comboBox1.SelectedItem.ToString() == (String)DataR["Ostan"] && comboBox2.SelectedItem.ToString() == (String)DataR["Shahr"])
                    {
                        comboBox3.Items.Add(DataR["Mantaghe"]);
                    }
                }
            }
            DataR.Close();
            con.Close();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox4.Text = "";
            comboBox4.Items.Clear();
            comboBox5.Text = "";
            comboBox5.Items.Clear();
            comboBox6.Text = "";
            comboBox6.Items.Clear();
            con.Close();
            con.Open();
            com = new SqlCommand("select distinct * from Melk where Reserved = 'no';", con);
            DataR = com.ExecuteReader();
            if (DataR.HasRows)
            {
                while (DataR.Read())
                {
                    if (comboBox1.SelectedItem.ToString() == (String)DataR["Ostan"]
                        && comboBox2.SelectedItem.ToString() == (String)DataR["Shahr"]
                        && comboBox3.SelectedItem.ToString() == (String)DataR["Mantaghe"]
                        )
                    {
                        comboBox4.Items.Add(DataR["Price"]);
                        comboBox5.Items.Add(DataR["Metrazh"]);
                        comboBox6.Items.Add(DataR["Sazeh"]);
                    }
                }
            }
            DataR.Close();
            con.Close();
        }
    }
}
