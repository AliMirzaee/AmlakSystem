﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AmlakSystem
{
    public partial class Account : Form
    {
        string b;
        public Account(string a)
        {
            InitializeComponent();
            b = a;
        }

        public Account()
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddMelk f1 = new AddMelk(textBox2.Text);
            f1.Show();
            this.Close();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void صفحهاصلیسایتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainPage f2 = new MainPage();
            f2.Show();
            this.Close();
        }

        private void پروفایلToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Profile f3 = new Profile();
            f3.Show();
            this.Close();
        }

        private void خروجToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainPage f4 = new MainPage();
            f4.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            EditMelk f5 = new EditMelk(dataGridView1.CurrentRow.Cells["PCode"].Value.ToString(),textBox2.Text);
            f5.Show();
            this.Close();
        }

        private void Account_Load(object sender, EventArgs e)
        {
            textBox2.Text = b;
            SqlConnection con = new SqlConnection("Data Source=Localhost;Initial Catalog=MoshaverAmlak;Integrated Security=True");
            con.Open();
            SqlCommand com = new SqlCommand("select * from Melk where Email=@p1", con);
            com.Parameters.Add(new SqlParameter("@p1", textBox2.Text));
            SqlDataReader DataR = com.ExecuteReader();
            if (DataR.HasRows)
            {
                while (DataR.Read())
                {
                    dataGridView1.Rows.Add(DataR["PCode"], DataR["Ostan"], DataR["Shahr"], DataR["Mantaghe"], DataR["Address"], DataR["Metrazh"], DataR["Bedroom Number"], DataR["Sazeh"], DataR["Price"], DataR["Reserved"]);
                }
            }
            DataR.Close();
            con.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount != 0)
            {
                DialogResult X;
                X = MessageBox.Show("آیا مطمئن هستید؟", "هشدار", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                string y = X.ToString();
                if (y == "Yes")
                {
                    SqlConnection con = new SqlConnection("Data Source=Localhost;Initial Catalog=MoshaverAmlak;Integrated Security=True");
                    con.Open();
                    SqlCommand com = new SqlCommand("select * from Melk", con);
                    SqlDataReader DataR = com.ExecuteReader();
                    while (DataR.Read())
                    {
                        if (DataR["PCode"].ToString() == dataGridView1.CurrentRow.Cells[0].Value.ToString())
                        {
                            DataR.Close();
                            con.Close();
                            SqlConnection con2 = new SqlConnection("Data Source=Localhost;Initial Catalog=MoshaverAmlak;Integrated Security=True");
                            con2.Open();
                            SqlCommand com2 = new SqlCommand("delete from Melk where PCode=@p1;", con2);
                            com2.Parameters.Add(new SqlParameter("@p1", dataGridView1.CurrentRow.Cells[0].Value.ToString()));
                            com2.ExecuteNonQuery();
                            dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
                            MessageBox.Show("ملک حذف شد");
                            con2.Close();
                            break;
                        }
                    }
                }

            }
            else
            {
                MessageBox.Show("موردی یافت نشد");
            }
        }
    }
}
