﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using AmlakSystem;

namespace Amlak_System
{
    public partial class Data : Form
    {
        SqlConnection con = new SqlConnection("Data Source=Localhost;Initial Catalog=MoshaverAmlak;Integrated Security=True");
        SqlCommand com;
        public SqlDataReader DataR2;
        public Data(SqlDataReader DataR)
        {
            DataR2 = DataR;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount != 0)
            {
                con.Close();
                con.Open();
                com = new SqlCommand("select * from Melk where Ostan=@p1 and Shahr=@p2 and Mantaghe=@p3 and Address=@p4 and Metrazh=@p5 and [Bedroom Number]=@p6 and Sazeh=@p7 and Price=@p8 and Reserved='no';", con);
                com.Parameters.Add(new SqlParameter("@p1", dataGridView1.CurrentRow.Cells[0].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p2", dataGridView1.CurrentRow.Cells[1].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p3", dataGridView1.CurrentRow.Cells[2].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p4", dataGridView1.CurrentRow.Cells[3].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p5", dataGridView1.CurrentRow.Cells[4].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p6", dataGridView1.CurrentRow.Cells[5].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p7", dataGridView1.CurrentRow.Cells[6].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p8", dataGridView1.CurrentRow.Cells[7].Value.ToString()));
                DataR2 = com.ExecuteReader();
                Melk_Data f2 = new Melk_Data(DataR2);
                f2.Show();
                this.Close();
            }
        }

        private void راهنماToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Data_Load(object sender, EventArgs e)
        {
            while (DataR2.Read())
            {
                dataGridView1.Rows.Add(DataR2["Ostan"], DataR2["Shahr"], DataR2["Mantaghe"], DataR2["Address"], DataR2["Metrazh"], DataR2["Bedroom Number"], DataR2["Sazeh"], DataR2["Price"]);
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void صفحهاصلیسایتToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            MainPage f1 = new MainPage();
            f1.Show();
            this.Close();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount != 0)
            {
                con.Close();
                con.Open();
                com = new SqlCommand("select * from Melk where Ostan=@p1 and Shahr=@p2 and Mantaghe=@p3 and Address=@p4 and Metrazh=@p5 and [Bedroom Number]=@p6 and Sazeh=@p7 and Price=@p8 and Reserved='no';", con);
                com.Parameters.Add(new SqlParameter("@p1", dataGridView1.CurrentRow.Cells[0].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p2", dataGridView1.CurrentRow.Cells[1].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p3", dataGridView1.CurrentRow.Cells[2].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p4", dataGridView1.CurrentRow.Cells[3].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p5", dataGridView1.CurrentRow.Cells[4].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p6", dataGridView1.CurrentRow.Cells[5].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p7", dataGridView1.CurrentRow.Cells[6].Value.ToString()));
                com.Parameters.Add(new SqlParameter("@p8", dataGridView1.CurrentRow.Cells[7].Value.ToString()));
                DataR2 = com.ExecuteReader();
                Melk_Data f2 = new Melk_Data(DataR2);
                f2.Show();
                this.Close();
            }
        }
    }
}
