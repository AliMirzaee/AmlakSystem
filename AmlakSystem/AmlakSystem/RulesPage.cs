﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmlakSystem
{
    public partial class RulesPage : Form
    {
        public RulesPage()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MainPage f2 = new MainPage();
            f2.Show();
            this.Close();
        }

        private void RulesPage_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Account f1 = new Account();
            f1.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RegisterPage f3 = new RegisterPage();
            f3.Show();
            this.Close();
        }
    }
}
