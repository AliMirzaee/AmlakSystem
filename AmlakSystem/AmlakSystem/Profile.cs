﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AmlakSystem
{
    public partial class Profile : Form
    {
        public Profile()
        {
            InitializeComponent();
        }

        private void Profile_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=Localhost;Initial Catalog=MoshaverAmlak;Integrated Security=True");
            con.Open();
            SqlCommand com = new SqlCommand("select * from Seller ;", con);
            SqlDataReader DataR = com.ExecuteReader();
            if (DataR.HasRows)
            {
                while (DataR.Read())
                {
                    textBox4.Text = (DataR["MelliCode"].ToString());
                    textBox5.Text = (DataR["Birthday"].ToString());
                }
            }
            DataR.Close();
            con.Close();
            con.Open();
            SqlCommand comm = new SqlCommand("select * from Shakhs ;", con);
            SqlDataReader DataRe = comm.ExecuteReader();
            if (DataRe.HasRows)
            {
                while (DataRe.Read())
                {
                    textBox1.Text = (DataRe["Full Name"].ToString());
                    textBox6.Text = (DataRe["Phone Number"].ToString());
                }
            }
            DataRe.Close();
            con.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Account f1 = new Account();
            f1.Show();
            this.Close();
        }
    }
}
