﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AmlakSystem
{
    public partial class AddMelk : Form
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-FGMP2O3;Initial Catalog=MoshaverAmlak;Integrated Security=True");
        string name;
        public AddMelk(string n)
        {
            InitializeComponent();
            name = n;
        }

        private void AddMelk_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == string.Empty || textBox3.Text == string.Empty || textBox4.Text == string.Empty || textBox5.Text == string.Empty || textBox2.Text == string.Empty || textBox6.Text == string.Empty || textBox7.Text == string.Empty || comboBox1.Text == string.Empty || textBox9.Text == string.Empty)
                MessageBox.Show("همه فیلد ها را پر کنید", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                try
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("insert into Melk (Ostan,Shahr,Mantaghe,[Address],PCode,Metrazh,[Bedroom Number],Sazeh,Price,Email,Reserved) values(@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8,@p9,@p10,@p11)", con);
                    com.Parameters.Add(new SqlParameter("@p1", textBox1.Text));
                    com.Parameters.Add(new SqlParameter("@p2", textBox3.Text));
                    com.Parameters.Add(new SqlParameter("@p3", textBox4.Text));
                    com.Parameters.Add(new SqlParameter("@p4", textBox5.Text));
                    com.Parameters.Add(new SqlParameter("@p5", textBox2.Text));
                    com.Parameters.Add(new SqlParameter("@p6", textBox6.Text));
                    com.Parameters.Add(new SqlParameter("@p7", textBox7.Text));
                    com.Parameters.Add(new SqlParameter("@p8", comboBox1.Text));
                    com.Parameters.Add(new SqlParameter("@p9", textBox9.Text));
                    com.Parameters.Add(new SqlParameter("@p10", name));
                    com.Parameters.Add(new SqlParameter("@p11", "no"));
                    com.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("ملک با موفقیت ثبت شد", "موفق", MessageBoxButtons.OK);
                    Account f1 = new Account(name);
                    f1.Show();
                    this.Close();
                }
                catch
                {
                    MessageBox.Show("این ملک قبلا ثبت شده", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Account f2 = new Account(name);
            f2.Show();
            this.Close();
        }
    }
}
