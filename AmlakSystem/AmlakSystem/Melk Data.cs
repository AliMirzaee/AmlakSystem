﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using AmlakSystem;

namespace Amlak_System
{
    public partial class Melk_Data : Form
    {
        SqlConnection con = new SqlConnection("Data Source=Localhost;Initial Catalog=MoshaverAmlak;Integrated Security=True");
        SqlCommand com;
        public SqlDataReader DataR3;
        public String ST;
        public String Nazar;
        public Melk_Data(SqlDataReader DataR)
        {
            DataR3 = DataR;
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MainPage MP = new MainPage();
            MP.Show();
            this.Close();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            Customer_Data CD = new Customer_Data(ST);
            CD.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                DialogResult X;
                X = MessageBox.Show("میخواهید نظر را ثبت کنید ؟", "پیغام", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                string y = X.ToString();
                if (y == "Yes")
                {
                    Login lg = new Login(ST, Nazar);

                    lg.Show();
                }
            }
        }

        private void Melk_Data_Load(object sender, EventArgs e)
        {
            DataR3.Read();
            textBox2.Text = DataR3["Ostan"].ToString();
            textBox3.Text = DataR3["Shahr"].ToString();
            textBox4.Text = DataR3["Mantaghe"].ToString();
            textBox5.Text = DataR3["Address"].ToString();
            textBox6.Text = DataR3["Metrazh"].ToString();
            textBox7.Text = DataR3["Bedroom Number"].ToString();
            textBox8.Text = DataR3["Sazeh"].ToString();
            textBox9.Text = DataR3["Price"].ToString();
            ST = DataR3["PCode"].ToString();
            con.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Nazar = textBox1.Text;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            MainPage MP = new MainPage();
            MP.Show();
            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                DialogResult X;
                X = MessageBox.Show("میخواهید نظر را ثبت کنید ؟", "پیغام", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                string y = X.ToString();
                if (y == "Yes")
                {
                    Login lg = new Login(ST, Nazar);

                    lg.Show();
                }
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Customer_Data CD = new Customer_Data(ST);
            CD.Show();
            this.Close();
        }
    }
}
