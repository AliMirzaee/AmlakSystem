﻿namespace Amlak_System
{
    partial class Data
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.استان = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.شهر = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.منطقه = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.آدرس = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.متراژ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.تعدادخواب = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.سازه = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.قیمت = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.صفحهاصلیسایتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.راهنماToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.استان,
            this.شهر,
            this.منطقه,
            this.آدرس,
            this.متراژ,
            this.تعدادخواب,
            this.سازه,
            this.قیمت});
            this.dataGridView1.Location = new System.Drawing.Point(13, 56);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(848, 170);
            this.dataGridView1.TabIndex = 6;
            // 
            // استان
            // 
            this.استان.HeaderText = "استان";
            this.استان.Name = "استان";
            this.استان.ReadOnly = true;
            // 
            // شهر
            // 
            this.شهر.HeaderText = "شهر";
            this.شهر.Name = "شهر";
            this.شهر.ReadOnly = true;
            // 
            // منطقه
            // 
            this.منطقه.HeaderText = "منطقه";
            this.منطقه.Name = "منطقه";
            this.منطقه.ReadOnly = true;
            // 
            // آدرس
            // 
            this.آدرس.HeaderText = "آدرس";
            this.آدرس.Name = "آدرس";
            this.آدرس.ReadOnly = true;
            // 
            // متراژ
            // 
            this.متراژ.HeaderText = "متراژ";
            this.متراژ.Name = "متراژ";
            this.متراژ.ReadOnly = true;
            // 
            // تعدادخواب
            // 
            this.تعدادخواب.HeaderText = "تعداد خواب";
            this.تعدادخواب.Name = "تعدادخواب";
            this.تعدادخواب.ReadOnly = true;
            // 
            // سازه
            // 
            this.سازه.HeaderText = "نوع ساختمان";
            this.سازه.Name = "سازه";
            this.سازه.ReadOnly = true;
            // 
            // قیمت
            // 
            this.قیمت.HeaderText = "قیمت";
            this.قیمت.Name = "قیمت";
            this.قیمت.ReadOnly = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.صفحهاصلیسایتToolStripMenuItem,
            this.راهنماToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.menuStrip1.Size = new System.Drawing.Size(873, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // صفحهاصلیسایتToolStripMenuItem
            // 
            this.صفحهاصلیسایتToolStripMenuItem.Name = "صفحهاصلیسایتToolStripMenuItem";
            this.صفحهاصلیسایتToolStripMenuItem.Size = new System.Drawing.Size(113, 20);
            this.صفحهاصلیسایتToolStripMenuItem.Text = "صفحه اصلی سایت";
            this.صفحهاصلیسایتToolStripMenuItem.Click += new System.EventHandler(this.صفحهاصلیسایتToolStripMenuItem_Click_1);
            // 
            // راهنماToolStripMenuItem
            // 
            this.راهنماToolStripMenuItem.Name = "راهنماToolStripMenuItem";
            this.راهنماToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.راهنماToolStripMenuItem.Text = "راهنما";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(289, 408);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(327, 36);
            this.button1.TabIndex = 11;
            this.button1.Text = "انتخاب";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(762, 294);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "ملک های پیشنهادی";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(763, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "ملک های یافت شده";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(13, 310);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(848, 92);
            this.dataGridView2.TabIndex = 8;
            // 
            // Data
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(873, 448);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView2);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Data";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "صفحه ملک های یافت شده";
            this.Load += new System.EventHandler(this.Data_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn استان;
        private System.Windows.Forms.DataGridViewTextBoxColumn شهر;
        private System.Windows.Forms.DataGridViewTextBoxColumn منطقه;
        private System.Windows.Forms.DataGridViewTextBoxColumn آدرس;
        private System.Windows.Forms.DataGridViewTextBoxColumn متراژ;
        private System.Windows.Forms.DataGridViewTextBoxColumn تعدادخواب;
        private System.Windows.Forms.DataGridViewTextBoxColumn سازه;
        private System.Windows.Forms.DataGridViewTextBoxColumn قیمت;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem صفحهاصلیسایتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem راهنماToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView2;
    }
}