﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Amlak_System
{
    public partial class Login : Form
    {
        SqlConnection con = new SqlConnection("Data Source=Localhost;Initial Catalog=MoshaverAmlak;Integrated Security=True");
        SqlCommand com;
        String st_login;
        String Nazar_login;

        public Login(String ST, String Nazar)
        {
            st_login = ST;
            Nazar_login = Nazar;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("insert into Nazar (PCode,Email,Nazar) values(@p1,@p2,@p3);", con);
                com.Parameters.Add(new SqlParameter("@p1", st_login));
                com.Parameters.Add(new SqlParameter("@p2", textBox1.Text));
                com.Parameters.Add(new SqlParameter("@p3", Nazar_login));
                com.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("نظر با موفقیت ثبت شد");
                this.Hide();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("insert into Nazar (PCode,Email,Nazar) values(@p1,@p2,@p3);", con);
                com.Parameters.Add(new SqlParameter("@p1", st_login));
                com.Parameters.Add(new SqlParameter("@p2", textBox1.Text));
                com.Parameters.Add(new SqlParameter("@p3", Nazar_login));
                com.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("نظر با موفقیت ثبت شد");
                this.Hide();
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
