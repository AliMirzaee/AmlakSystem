﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using AmlakSystem;

namespace Amlak_System
{
    public partial class Customer_Data : Form
    {
        SqlConnection con = new SqlConnection("Data Source=Localhost;Initial Catalog=MoshaverAmlak;Integrated Security=True");
        SqlCommand com;
        SqlCommand com2;
        public SqlDataReader DataR4;
        public String ST1;
        public String Email;
        public Customer_Data(String ST)
        {
            ST1 = ST;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox4.Text != "" && textBox9.Text != "")
            {
                con.Close();
                con.Open();
                com = new SqlCommand("insert into Shakhs(Email,Type,[Full Name],[Phone Number]) values(@p1,'2',@p2,@p3);", con);
                com.Parameters.Add(new SqlParameter("@p1", textBox9.Text));
                com.Parameters.Add(new SqlParameter("@p2", textBox1.Text));
                com.Parameters.Add(new SqlParameter("@p3", textBox4.Text));
                com.ExecuteNonQuery();
                com2 = new SqlCommand("insert into Kharidar values(@p1);", con);
                com2.Parameters.Add(new SqlParameter("@p1", textBox9.Text));
                com2.ExecuteNonQuery();
                Email = textBox9.Text;
                MPayment MPf = new MPayment(ST1, Email);
                MPf.Show();
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MainPage MP = new MainPage();
            MP.Show();
            this.Close();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            MainPage MP = new MainPage();
            MP.Show();
            this.Close();
        }

        private void Customer_Data_Load(object sender, EventArgs e)
        {

        }
    }
}
