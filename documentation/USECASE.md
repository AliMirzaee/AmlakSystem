# فاز صفحه اصلی
![Usecase Diagram](documentation/images/Usecase Diagram1.png)
----
# فاز صفحه انتخاب نوع اشتراک و  قوانین و ثبت نام
![Usecase Diagram](documentation/images/Usecase%20Diagram2.png)
----
# فاز صفحه کاربری و ثبت ملک و ویرایش ملک و پروفایل
![Usecase Diagram](documentation/images/Usecase%20Diagram3.png)
----
# فاز صفحه ملک های یافت شده و مشخصات ملک و رزرو ملک و اطلاعات خریدار
![Usecase Diagram](documentation/images/Usecase%20Diagram4.png)