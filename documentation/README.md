# مستندات پروژه
* [سناریوی پروژه](https://gitlab.com/AliMirzaee/AmlakSystem/blob/master/documentation/SCENARIO.md)
* [مدل سازی با استفاده از Use Case بر اساس سناریو](https://gitlab.com/AliMirzaee/AmlakSystem/blob/master/documentation/USECASE.md)
* [نیازمندی های پروژه](https://gitlab.com/AliMirzaee/AmlakSystem/blob/master/documentation/REQUIERMENTS.md)